package Snakepong3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")
/**
 * @author
 */
public class RenderPanel extends JPanel
{

	public static final Color GREEN = new Color(1666073);

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		SnakePong snakePong = SnakePong.snakePong;

		g.setColor(GREEN);
		
		g.fillRect(0, 0, 800, 700);

		g.setColor(Color.BLUE);

		for (Point point : snakePong.snakeParts)
		{
			g.fillRect(point.x * SnakePong.SCALE, point.y * SnakePong.SCALE, SnakePong.SCALE, SnakePong.SCALE);
		}
		
		g.fillRect(SnakePong.snakeParts.getHeadPoint().x * SnakePong.SCALE, snake.head.y * SnakePong.SCALE, SnakePong.SCALE, SnakePong.SCALE);
		
		g.setColor(Color.RED);
		
		g.fillRect(snake.cherry.x * SnakePong.SCALE, snake.cherry.y * SnakePong.SCALE, SnakePong.SCALE, SnakePong.SCALE);
		
		String string = "Score: " + snakePong.score + ", Length: " + snakePong.taillength + ", Time: " + snakePong.time / 20;
		
		g.setColor(Color.white);
		
		g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), 10);

		string = "Game Over!";

		if (snakePong.over)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snakePong.dim.getHeight() / 4);
		}

		string = "Paused!";

		if (snakePong.paused && !snakePong.over)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snakePong.dim.getHeight() / 4);
		}
	}
}