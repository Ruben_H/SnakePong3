package Snakepong3;

import java.util.Iterator;

import java.awt.Point;
@SuppressWarnings("hiding")

public class DoublyLinkedList<Point> implements Iterable<Point>{
	private Node head = null;
	private Node tail = null;
	private int size = 0;
	private int xVals[];
	private int yVals[];
	
	public DoublyLinkedList(){
		this.head = null;
		this.tail = null;
		this.size = 0;
	}
	
	public DoublyLinkedList(Point point){
		
		this.head = new Node(point);
		this.tail = new Node(point);
		this.size = 1;
	}
	
	public Node getHead() {
		return head;
	}
	public Node getTail() {
		return tail;
	}
	public Point getHeadPoint() {
		return head.getPoint();
	}
	public Point getTailPoint() {
		return tail.getPoint();
	}
	
	public void setTail(Node node) {
		this.tail = node;
	}
	
	public void setHead(Node node) {
		this.head = node;
	}
	public void append(Point point) {
		Node temp = this.getHead();
		Node node = new Node(point);
		this.head = node;
		node.setPre(null);
		node.setPost(temp.getPoint());
		temp.setPre(node.getPoint());
		size++;
		}
	
	public void appendXY(int x, int y) {
		Point point = new Point(x,y);
		this.append(point);
	}
	
	public void removeTail() {
		Node node = this.getTail();
		this.getTail().getPre().setPost(null);
		this.setTail(node.getPre());
		node.setPre(null);
		node.setPost(null);
		node = null;
		size--;
	}
	public int getSize() {
		return size;
	}
	public void clear() {
	
		
	}
	public boolean noListAt(int x, int y) {
		Point temp = new Point (x,y);
		int i= 0;
		boolean bool = false;
		Node cursor = this.getHead();
		while (i<= this.getSize()) {
			if(cursor.getPoint() == temp) {
				bool = true;
				
			}
			else {
				cursor = cursor.getPre();
				i++;
				bool = false;
			}
			return bool;
		}
	}
		
	
	
	
	
		
		
	private class Node {
		Point point;
		Node pre;
		Node post;
		
		public Node (Point point2) {
			this.point = point2;
		}
		public Node (Point point, Point pre, Point post) {
			this.point = point;
			this.pre = new Node(pre);
			this.post = new Node(post);
		}
		
		public Node (int x,int y) {
			this.point = new Point(x,y);
		}
		
		public Node getPre() {
			return pre;
		}
		public Node getPost() {
			return post;
		}
		public void setPre(Point point) {
			this.pre = new Node(point);
		}
		public void setPost(Point point) {
			this.post = new Node(point);
		}
		public Point getPoint() {
			return point;
		}
		
		
		
		
	}
	@Override
	public Iterator<Point> iterator() {
		return new DoublyLinkedListIterator();
	}
	
	private class DoublyLinkedListIterator implements Iterator<Point>{
		private Node cursor = head;
		@Override
		public boolean hasNext() {
			return cursor != null;
		}

		@Override
		public Point next() {
			Point point = cursor.getPoint();
			cursor = cursor.getPost();
			return point;
		}
	
	}
}







	
	

	



