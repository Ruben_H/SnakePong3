package Snakepong3;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;




public class SnakePong extends DoublyLinkedList<Point> implements ActionListener, KeyListener {
	
	public static SnakePong snakePong;
	
	public JFrame jframe;
	
	public RenderPanel renderPanel;
	
	public Timer timer;
	
	public boolean over = false , paused = false;
	
	public DoublyLinkedList<Point> snakeParts = new DoublyLinkedList<Point>();
	
	public static final int UP=0, DOWN=1, LEFT=2, RIGHT=3, SCALE=10;
	
	public int ticks = 0, direction = DOWN, score, taillength = 10, time;
	
	public Point cherry;
	
	public Random random;
	
	public Dimension dim;
	
	
	
	public SnakePong() {
		dim= Toolkit.getDefaultToolkit().getScreenSize();
		jframe = new JFrame ("SnakePong");
		jframe.setVisible(true);
		jframe.setSize(805, 700);
		jframe.setResizable(false);
		jframe.setLocation(dim.width /2 - jframe.getWidth() / 2, dim.height/2 -jframe.getHeight()/ 2);
		jframe.add(renderPanel = new RenderPanel());
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.addKeyListener(this);
		startGame();
		
	}
	
	public void startGame() {
		
		time = 0;
		score = 0;
		taillength = 14;
		ticks = 0;
		direction = DOWN;
		
		random = new Random();
		snakeParts.clear();
		cherry = new Point(random.nextInt(79), random.nextInt(66));
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		renderPanel.repaint();
		ticks++;
		
		if (ticks % 2 == 0 && snakeParts.getHeadPoint() != null) {
			time++;
			
			if (direction == UP) {
				if (snakeParts.getHeadPoint().y -1 <=0 && noSnakeAt(snakeParts.getHeadPoint().x, snakeParts.getHeadPoint().y -1) ) {
					over = false;
				}
				else over = true;
			}
			
			if (direction == DOWN) {
				if (snakeParts.getHeadPoint().y +1 <=700 && noSnakeAt(snakeParts.getHeadPoint().x, snakeParts.getHeadPoint().y +1) ) {
					over = false;
				}
				else over = true;
			}
			if (direction == LEFT) {
				
				if (snakeParts.getHeadPoint().x -1 <=0 && noSnakeAt(snakeParts.getHeadPoint().x-1, snakeParts.getHeadPoint().y) ) {
					over = false;
				}
				else over = true;
			}
			if (direction == RIGHT) {
				if (snakeParts.getHeadPoint().x +1 <=805 && noSnakeAt(snakeParts.getHeadPoint().x+1, snakeParts.getHeadPoint().y) ) {
					over = false;
				}
				else over = true;
			}
			if (cherry != null) {
				if (snakeParts.getHeadPoint() == cherry) {
					score+=10;
					snakeParts.appendXY(snakeParts.getHeadPoint().x+1, snakeParts.getHeadPoint().y) ;
					cherry.setLocation(random.nextInt(79), random.nextInt(66));
				}
			}
			
		}
		
	}
	public boolean noSnakeAt(int x, int y) {
		return this.noListAt(x, y);
	}
	
	
	public static void main(String[] args)
	{
		snakePong = new SnakePong();
	}
	
	
	

	@Override
	public void keyPressed(KeyEvent e)
	{
		int i = e.getKeyCode();

		if ((i == KeyEvent.VK_A || i == KeyEvent.VK_LEFT) && direction != RIGHT)
		{
			direction = LEFT;
		}

		if ((i == KeyEvent.VK_D || i == KeyEvent.VK_RIGHT) && direction != LEFT)
		{
			direction = RIGHT;
		}

		if ((i == KeyEvent.VK_W || i == KeyEvent.VK_UP) && direction != DOWN)
		{
			direction = UP;
		}

		if ((i == KeyEvent.VK_S || i == KeyEvent.VK_DOWN) && direction != UP)
		{
			direction = DOWN;
		}

		if (i == KeyEvent.VK_SPACE)
		{
			if (over)
			{
				startGame();
			}
			else
			{
				paused = !paused;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}



	
	

}
