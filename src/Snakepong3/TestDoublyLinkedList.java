package Snakepong3;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;


class TestDoublyLinkedList {
	
	DoublyLinkedList<Point> ll1;
	
	@BeforeEach
	void setUp() {
		Point point = new Point(10,10);
		ll1 = new DoublyLinkedList<Point> (point);
	}
	
	@Test
	void testGetHeadPoint() {
		Point point = new Point(20,30);
		DoublyLinkedList<Point> ll = new DoublyLinkedList<Point>(point);
		assertEquals(10, (int) ll1.getHeadPoint().getX());
		assertEquals(10, (int) ll1.getHeadPoint().getY());
		assertEquals(20, (int) ll.getHeadPoint().getX());
		assertEquals(30, (int) ll.getHeadPoint().getY());
	
	}
	
	@Test
	void testGetTailPoint() {
		Point point = new Point(20,30);
		DoublyLinkedList<Point> ll = new DoublyLinkedList<Point>(point);
		assertEquals(10, (int) ll1.getHeadPoint().getX());
		assertEquals(10, (int) ll1.getHeadPoint().getY());
		assertEquals(20, (int) ll.getHeadPoint().getX());
		assertEquals(30, (int) ll.getHeadPoint().getY());
		
	}
	
	@Test
	void testAppend() {
		Point point = new Point(20,30);
		ll1.append(point);
		assertEquals(20, (int) ll1.getHeadPoint().getX());
		assertEquals(30, (int) ll1.getHeadPoint().getY());
		
	}
	

	
}
